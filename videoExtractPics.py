import os

data_path = "../data/doors_stairs/data/Stair-2-NewData"


def frameByFrameExtractPics(mov_path):
    cmd = 'ffmpeg -i ' + mov_path + ' ' + mov_path[:-4] + '_%04d.jpg -hide_banner'
    os.system(cmd)
    return 0


def batachExtract(data_dir):
    for root, dirs, files in os.walk(data_dir):
        for mov_file in files:
            if '.MTS' in mov_file:
                mov_path = os.path.join(root, mov_file)
                frameByFrameExtractPics(mov_path)
    return 0


# batachExtract('../data/doors_stairs/dis_video')
