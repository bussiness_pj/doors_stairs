/usr/bin/python2.7 /home/zhangxulong/project/doors_stairs/buildModel.py
Using TensorFlow backend.
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcublas.so.8.0 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcudnn.so.5 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcufft.so.8.0 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcurand.so.8.0 locally
Found 33215 images belonging to 2 classes.
Found 5720 images belonging to 2 classes.
Epoch 1/50
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE3 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:910] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_device.cc:885] Found device 0 with properties: 
name: GeForce GTX 1070
major: 6 minor: 1 memoryClockRate (GHz) 1.8225
pciBusID 0000:01:00.0
Total memory: 7.92GiB
Free memory: 7.42GiB
I tensorflow/core/common_runtime/gpu/gpu_device.cc:906] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:916] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:975] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GeForce GTX 1070, pci bus id: 0000:01:00.0)
13s - loss: 0.5415 - acc: 0.7430 - val_loss: 0.0302 - val_acc: 1.0000
Epoch 2/50
12s - loss: 0.1991 - acc: 0.9440 - val_loss: 0.0102 - val_acc: 1.0000
Epoch 3/50
13s - loss: 0.1240 - acc: 0.9630 - val_loss: 0.0446 - val_acc: 0.9900
Epoch 4/50
12s - loss: 0.0924 - acc: 0.9750 - val_loss: 1.2223e-04 - val_acc: 1.0000
Epoch 5/50
12s - loss: 0.1147 - acc: 0.9810 - val_loss: 3.2180e-05 - val_acc: 1.0000
Epoch 6/50
12s - loss: 0.1921 - acc: 0.9690 - val_loss: 0.0560 - val_acc: 0.9950
Epoch 7/50
13s - loss: 0.1204 - acc: 0.9720 - val_loss: 9.3598e-07 - val_acc: 1.0000
Epoch 8/50
13s - loss: 0.1340 - acc: 0.9730 - val_loss: 1.0233e-04 - val_acc: 1.0000
Epoch 9/50
12s - loss: 0.1662 - acc: 0.9730 - val_loss: 4.3217e-06 - val_acc: 1.0000
Epoch 10/50
13s - loss: 0.1599 - acc: 0.9860 - val_loss: 1.0903e-07 - val_acc: 1.0000
Epoch 11/50
12s - loss: 0.2406 - acc: 0.9710 - val_loss: 0.0028 - val_acc: 1.0000
Epoch 12/50
13s - loss: 0.1106 - acc: 0.9820 - val_loss: 1.0989e-07 - val_acc: 1.0000

Process finished with exit code 0
