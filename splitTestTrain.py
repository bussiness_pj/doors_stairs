import os
from baseZhang import if_no_create_it


def split_train_test(data_dir):
    for root, dirs, files in os.walk(data_dir):
        for jpg_file in files:
            jpg_path = os.path.join(root, jpg_file)
            if '.jpg' in jpg_path:
                if '000' in jpg_path:
                    new_path = jpg_path.replace('allPics', 'test')

                else:
                    new_path = jpg_path.replace('allPics', 'train')
                if_no_create_it(new_path)
                os.rename(jpg_path, new_path)
    return 0


# split_train_test("../data/doors_stairs/allPics")

def rename_train_test(data_dir):
    for root, dirs, files in os.walk(data_dir):
        for jpg_file in files:
            jpg_path = os.path.join(root, jpg_file)
            if '.jpg' in jpg_path:
                folder = jpg_path.split('/')[-2]
                if "Door" in folder:
                    new_path = jpg_path.replace(folder, 'doors')
                elif "Stair" in folder:
                    new_path = jpg_path.replace(folder, 'stairs')
                if_no_create_it(new_path)
                os.rename(jpg_path, new_path)
    return 0


# rename_train_test('../data/doors_stairs/train')


def rename_train_test_for_distance(data_dir):
    for root, dirs, files in os.walk(data_dir):
        for jpg_file in files:
            jpg_path = os.path.join(root, jpg_file)
            if '.jpg' in jpg_path:
                folder = jpg_path.split('/')[-2]
                distance = jpg_path.split('_')[-2]
                if_no_create_it(jpg_path.replace(folder, distance))
                # os.rename(jpg_path, jpg_path.replace(folder, distance))#TODO bug
    return 0

# rename_train_test_for_distance('../data/doors_stairs/train_distance')