/usr/bin/python2.7 /home/zhangxulong/project/doors_stairs/distanceModel/buildCNN.py
Using TensorFlow backend.
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcublas.so.8.0 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcudnn.so.5 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcufft.so.8.0 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:135] successfully opened CUDA library libcurand.so.8.0 locally
Found 33215 images belonging to 9 classes.
Found 5720 images belonging to 9 classes.
Epoch 1/50
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE3 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:910] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_device.cc:885] Found device 0 with properties: 
name: GeForce GTX 1070
major: 6 minor: 1 memoryClockRate (GHz) 1.8225
pciBusID 0000:01:00.0
Total memory: 7.92GiB
Free memory: 7.51GiB
I tensorflow/core/common_runtime/gpu/gpu_device.cc:906] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:916] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:975] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GeForce GTX 1070, pci bus id: 0000:01:00.0)
25s - loss: 2.2168 - acc: 0.1290 - val_loss: 2.1727 - val_acc: 0.2189
Epoch 2/50
24s - loss: 2.1796 - acc: 0.1570 - val_loss: 1.9993 - val_acc: 0.2478
Epoch 3/50
24s - loss: 1.9510 - acc: 0.2910 - val_loss: 1.5487 - val_acc: 0.4808
Epoch 4/50
24s - loss: 1.6197 - acc: 0.4070 - val_loss: 1.2699 - val_acc: 0.5754
Epoch 5/50
24s - loss: 1.4843 - acc: 0.4660 - val_loss: 0.9153 - val_acc: 0.6516
Epoch 6/50
24s - loss: 1.2375 - acc: 0.5660 - val_loss: 0.9474 - val_acc: 0.6531
Epoch 7/50
24s - loss: 1.1124 - acc: 0.6130 - val_loss: 0.8972 - val_acc: 0.6864
Epoch 8/50
24s - loss: 1.0878 - acc: 0.6230 - val_loss: 0.9658 - val_acc: 0.6768
Epoch 9/50
24s - loss: 0.9802 - acc: 0.6660 - val_loss: 0.8910 - val_acc: 0.6982
Epoch 10/50
24s - loss: 0.8949 - acc: 0.6870 - val_loss: 0.8498 - val_acc: 0.7167
Epoch 11/50
24s - loss: 0.8498 - acc: 0.7190 - val_loss: 0.9583 - val_acc: 0.6945
Epoch 12/50
24s - loss: 0.8971 - acc: 0.7070 - val_loss: 0.7610 - val_acc: 0.7478
Epoch 13/50
24s - loss: 0.8182 - acc: 0.7400 - val_loss: 0.9353 - val_acc: 0.7101
Epoch 14/50
24s - loss: 0.7787 - acc: 0.7300 - val_loss: 0.8776 - val_acc: 0.7367
Epoch 15/50
24s - loss: 0.8057 - acc: 0.7290 - val_loss: 0.8448 - val_acc: 0.6975
Epoch 16/50
24s - loss: 0.6957 - acc: 0.7640 - val_loss: 0.8775 - val_acc: 0.7130
{'11': 1, '10': 0, '13': 3, '12': 2, '15': 5, '14': 4, '7': 6, '9': 8, '8': 7}

Process finished with exit code 0
